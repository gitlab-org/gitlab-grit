# encoding: UTF-8

require File.dirname(__FILE__) + '/helper'

class TestEncoding < Test::Unit::TestCase
  def test_nil_message
    message = GritExt.encode! nil
    assert_nil message
  end

  def test_binary_message
    message = "\xFF\xD8\xFF\xE0"
    encoded_message = GritExt.encode!(message)
    assert_equal message.bytes.to_a, encoded_message.bytes.to_a
    assert_equal 'ASCII-8BIT', message.encoding.name
  end

  def test_invalid_encoding
    message = GritExt.encode!("yummy\xE2 \xF0\x9F\x8D\x94 \x9F\x8D\x94")
    assert_equal '--broken encoding: UTF-16BE', message
    assert_equal 'UTF-8', message.encoding.name
  end

  def test_encode_string
    message = GritExt.encode!("{foo \xC3 'bar'}")
    assert_equal "{foo Ã 'bar'}", message
    assert_equal 'UTF-8', message.encoding.name

    message = "我爱你".encode('GBK')
    assert_equal 'GBK', message.encoding.name

    encoded_message = GritExt.encode!(message)
    assert_equal '--broken encoding: UTF-16BE', encoded_message
    assert_equal 'UTF-8', encoded_message.encoding.name
  end
end

